variable "subnet_ids" {
  type        = list
}

variable "vpc_id" {}

variable "db_name" {}
variable "cidr_blocks" {
  type        = list
}
