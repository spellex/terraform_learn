variable "subnet_ids" {
  type        = list
}

variable "vpc_id" {}

variable "cidr_blocks" {
  type        = list
}
