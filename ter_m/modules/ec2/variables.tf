variable "aws_region" {
    default = "us-west-2"
}
variable "ec2_count" {
  default = "1"
}
variable "ami_id" {
    // Ubuntu Server 18.04 LTS (HVM), SSD Volume Type in us-west-2
    default = "ami-0d1cd67c26f5fca19"
}
variable "instance_type" {
  default = "t2.micro"
}

variable "subnet_public_id" {}
variable "vpc_id" {}
variable "key_name" {}
variable "db-endpoint" {}
variable "ec2_name" {}

variable "elb_port" {
  description = "The port the elb will be listening"
  type        = number
  default     = 80
}

variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG"
  type        = number
  default     = 1
}

variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG"
  type        = number
  default     = 1
}

variable "desired_capacity" {
  description = "The desired number of EC2 Instances in the ASG"
  type        = number
  default     = 1
}

variable "server_port" {
  description = "The port the web server will be listening"
  type        = number
  default     = 80
}
