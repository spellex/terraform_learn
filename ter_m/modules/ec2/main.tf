data "aws_availability_zones" "available" {
  state = "available"
}

#############################################################################
# Configuration for new instance to autoscaling group
#############################################################################

resource "aws_launch_configuration" "asg-launch-config-sample" {
    image_id           = var.ami_id
    instance_type = var.instance_type
    key_name      = var.key_name
    security_groups = [ aws_security_group.ec2-sg.id ]
    associate_public_ip_address = true

    lifecycle {
      create_before_destroy = true
    }


depends_on = [ var.db-endpoint ]

    user_data = templatefile("user-data.sh.tmpl",{ db_endpoint = var.db-endpoint })
}

#############################################################################
# Security group server (load balancer listen port)
#############################################################################

resource "aws_security_group" "elb-sg" {
  name = "${var.ec2_name}-elb-sg"
  vpc_id = var.vpc_id


  ingress {
    from_port   = var.elb_port
    to_port     = var.elb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#############################################################################
# Security group server (instance)
#############################################################################

resource "aws_security_group" "ec2-sg" {
  name        = "${var.ec2_name}-sg"
  description = "allow inbound access to the EC2 instance"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#############################################################################
#Autoscaling_group
#############################################################################

resource "aws_autoscaling_group" "asg-sample" {
  launch_configuration = aws_launch_configuration.asg-launch-config-sample.id
  min_size             = var.min_size
  max_size             = var.max_size
  desired_capacity     = var.desired_capacity

  vpc_zone_identifier  = [var.subnet_public_id]

  load_balancers    = [aws_elb.sample.name]
  health_check_type = "ELB"

  tag {
    key                 = "Name"
    value               = "${var.ec2_name}-asg"
    propagate_at_launch = true
  }

}


#############################################################################
# Load balancer
#############################################################################
resource "aws_elb" "sample" {
  name               = "${var.ec2_name}-asg-elb"
  security_groups    = [aws_security_group.elb-sg.id]
  subnets = [var.subnet_public_id]

  health_check {
    target              = "HTTP:${var.server_port}/intermine-demo/begin.do"
    interval            = 300
    timeout             = 3
    healthy_threshold   = 3
    unhealthy_threshold = 6
  }

  cross_zone_load_balancing   = true

  listener {
    lb_port           = var.elb_port
    lb_protocol       = "http"
    instance_port     = var.server_port
    instance_protocol = "http"
  }
}
