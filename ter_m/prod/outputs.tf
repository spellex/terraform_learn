output "elb_dns_name" {
  value       = module.ec2.elb_dns_name
  description = "The domain name of the load balancer"
}
