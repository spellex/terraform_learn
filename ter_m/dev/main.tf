provider "aws" {
  region = "us-east-2"
}

#############################################################################
# Availability zones
#############################################################################

data "aws_availability_zones" "available" {
  state = "available"
}

#############################################################################
# VPC
#############################################################################

module "vpc" {
    source = "../modules/vpc"
    vpc_name = "dev_vpc"
    vpc_cidr = "192.168.0.0/16"
    public_cidr = "192.168.1.0/24"
    private1_cidr = "192.168.2.0/24"
    private2_cidr = "192.168.3.0/24"
    private1_az = data.aws_availability_zones.available.names[0]
    private2_az = data.aws_availability_zones.available.names[1]
    public_az = data.aws_availability_zones.available.names[0]
}

#############################################################################
# RDS
#############################################################################

module "rds_pgsql" {
    source  = "../modules/rds_pgsql"
    db_name = "devpgsql"
    subnet_ids = [ module.vpc.subnet_private1_id, module.vpc.subnet_private2_id]
    vpc_id = module.vpc.vpc_id
    cidr_blocks = [ module.vpc.vpc_cidr ]
}
module "ec2" {
  source               = "../modules/ec2"
  subnet_public_id = module.vpc.subnet_public_id
  vpc_id = module.vpc.vpc_id
  db-endpoint = module.rds_pgsql.db-endpoint
  ec2_name = "dev"
  ami_id    = "ami-0dd9f0e7df0f0a138"
  instance_type  = "t2.micro"
  key_name = "learn_aws"
  server_port = 8080
}
