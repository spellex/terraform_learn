#!/bin/sh
sudo apt-get update \
  && sudo apt-add-repository --yes --update ppa:ansible/ansible \
  && sudo apt update -y \
  && sudo apt-get install git software-properties-common ansible mc -y \
  && ansible-galaxy install geerlingguy.docker \
  && git clone https://gitlab.com/spellex/terraform_learn.git /opt/terraform \
  && ansible-playbook -vvv --extra-vars "name_server=jenkins" --connection=local /opt/terraform/ansible/project.yml
