provider "aws" {
  region = "us-east-2"
}

#############################################################################
# Availability zones
#############################################################################

data "aws_availability_zones" "available" {
  state = "available"
}

#############################################################################
# VPC
#############################################################################

module "vpc" {
    source = "../modules/vpc"
    vpc_name = "jenkins_vpc"
    vpc_cidr = "192.168.0.0/16"
    public_cidr = "192.168.4.0/24"
    private1_cidr = "192.168.5.0/24"
    private2_cidr = "192.168.6.0/24"
    private1_az = data.aws_availability_zones.available.names[0]
    private2_az = data.aws_availability_zones.available.names[1]
    public_az = data.aws_availability_zones.available.names[0]
}


module "jenkins" {
  source               = "../modules/jenkins"
  subnet_public_id = module.vpc.subnet_public_id
  vpc_id = module.vpc.vpc_id
  ec2_name = "jenkins"
  ami_id    = "ami-0dd9f0e7df0f0a138"
  instance_type  = "t2.micro"
  key_name = "learn_aws"
}
